const express = require('express');
const dotenv = require('dotenv');
const res = require('express/lib/response');

//Configuration the .env file
dotenv.config();

//create Express App
const app = express();
const port = process.env.PORT || 8000;

//Define the firs Route of App
app.get('/', (req, res) => {
    //Send Hello Word
    res.send('Welcome to Api Restful: Express + Ts + Swagger +Mongoose');
});
     //Execute App and Listen Request to PORT
    app.listen(port,() => {
        console.log('EXPRESS SERVER: Running at http://localhost:${port}')
    })