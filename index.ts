import express, { Express, Request, Response} from "express";
import dotenv from 'dotenv';


//Configuration the .env file
dotenv.config();

//Create Express App
const app: Express = express();
const port: string | number = process.env.PORT || 8000;

//Define the first Route of App
app.get('/', (req: Request, res: Response) => {
    //Send Hello Word
    res.send('Welcome to Api Restful: Express + Ts + Swagger +Mongoose');
});
 //Execute App and Listen Request to PORT
 app.listen(port,() => {
    console.log('EXPRESS SERVER: Running at http://localhost:${port}')
})